package net.test;

import com.test.actions.Actions;
import com.test.base.BaseTest;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class FirstTest extends BaseTest {
    static String[] text = {"Apple iPhone 7 Plus 32GB Jet Black"};
    static ArrayList<String> products = new ArrayList<>(Arrays.asList(text));

    @Test
    public void rozetkaSearch() throws InterruptedException {
        //open base page
        Actions.mainActions().openMainPage();

        //type query in field and submit query
        Actions.mainActions().searchProduct(products.get(0));

        //assert name of product and entered text
        Assert.assertTrue(Actions.mainActions().getName().contains(products.get(0)),"Assert name of product");

        //add item to cart
        Actions.mainActions().buy();

        //assert name in cart
        Assert.assertEquals(Actions.mainActions().getCurrentNameInCart(),products,"Assert list of product");

        //click to close popup window
        Actions.mainActions().closeWindow();

        // open basket
        Actions.mainActions().openBasket();

        //assert name in cart
        Assert.assertEquals(Actions.mainActions().getCurrentNameInCart(),products,"Assert list of product");

        //removeFromBasket item from cart
        Actions.mainActions().removeProduct();

        //agree to removeFromBasket from cart
        Actions.mainActions().agreeToRemove();

        //assert empty basket
        Assert.assertTrue(Actions.mainActions().checkEmptyBasket(),"Basket is empty");

        //close popup window
        Actions.mainActions().closeWindow();

        //assert name of item
        Assert.assertTrue(Actions.mainActions().getName().contains(products.get(0)),"Assert name of product");

        //refresh window
        Actions.mainActions().refresh();

        //assert name of item
        Assert.assertTrue(Actions.mainActions().getName().contains(products.get(0)),"Assert name of product");

        //open basket
        Actions.mainActions().openBasket();

        //Assert product is removed
        Assert.assertTrue(Actions.mainActions().checkProductRemoved(),"Product removed");

        //assert empty basket
        Assert.assertTrue(Actions.mainActions().checkEmptyBasket(),"Basket is empty");

        //close window
        Actions.mainActions().closeWindow();

        //assert name of item
        Assert.assertTrue(Actions.mainActions().getName().contains(products.get(0)),"Assert name of product");
    }

}
