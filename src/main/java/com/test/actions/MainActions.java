package com.test.actions;

import com.test.base.BaseActions;
import com.test.pages.Pages;

import java.util.ArrayList;

public class MainActions extends BaseActions {

    public void openMainPage(){
        openBeseUrl();
    }

    public void searchProduct(String text) {
        Pages.searchPage().typeTextAndSubmit(text);
    }

    public String getName(){
        return Pages.searchPage().getNameOfSearchingProduct();
    }

    public void buy(){
        Pages.searchPage().addToCart();
    }

    public ArrayList<String> getCurrentNameInCart(){
        return  Pages.searchPage().getNameInCart();
    }

    public void closeWindow(){
        Pages.searchPage().clickToClosePopupWindow();
    }

    public void openBasket(){
        Pages.searchPage().openBasket();
    }

    public void removeProduct(){
        Pages.searchPage().removeFromBasket();
    }

    public void agreeToRemove(){Pages.searchPage().agreeToRemove();}

    public boolean checkEmptyBasket(){return Pages.searchPage().isBasketEmpty();}

    public boolean checkProductRemoved(){return Pages.searchPage().isProductRemoved();}

    public void refresh(){BaseActions.refreshCurrentPage();}
}
