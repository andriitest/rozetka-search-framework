package com.test.pages;


import com.test.base.BasePage;
import com.test.locators.*;
import com.test.util.reporter.Reporter;

import java.util.ArrayList;

public class SearchPage extends BasePage {

    Locator searchField = new ClassName("rz-header-search-input-text");

    Locator nameofProduct = new XPath("//h1");

    Locator buyButton = new Name("topurchases");

    Locator elementOnPopup = new ID("cart_payment_info");

    Locator itemInCartName = new XPath("//div[@class='cart-i-title']/a[@name='goods-link']");

    Locator basketButton = new XPath("//div[contains(@id,'cart_block')]");

    Locator removeButton = new Name("before_delete");

    Locator agreeRemoveButton = new Name("delete");

    Locator textOfEmptyBasket = new XPath("//h2[contains(@class,'empty-cart-title')]");

    Locator closeWindowButton = new XPath("//a[@class='popup-close']");




    public void typeTextAndSubmit(String text){
        waitForElementVisibility(searchField);
        type("Input query: "+text,text,searchField);
        submit("Submit query ",searchField);
    }

    public String getNameOfSearchingProduct(){
        return getElementText("You assert for name of item",nameofProduct);
    }

    public void addToCart(){
        waitForElementVisibility(buyButton);
        waitForElementToBeClickable(buyButton);
        click("You click on button \"Buy\"",buyButton);
    }

    public ArrayList<String> getNameInCart(){
        waitForElementPresent(elementOnPopup);
        return getCurrentElementsText(itemInCartName);

    }

    public void clickToClosePopupWindow(){
        wait(2);
        waitForElementPresent(closeWindowButton);
        waitForElementToBeClickable(closeWindowButton);
        click("You click on button \"close\"",closeWindowButton);
    }

    public void openBasket(){
        waitForElementVisibility(basketButton);
        waitForElementToBeClickable(basketButton);
        click("You click on button \"Basket\"", basketButton);
    }

    public void removeFromBasket(){
        waitForElementToBeClickable(removeButton);
        click("Click on remove button", removeButton);
    }

    public void agreeToRemove(){
        waitForElementToBeClickable(agreeRemoveButton);
        click("You remove from Basket ",agreeRemoveButton);
    }

    public boolean isProductRemoved(){
        Reporter.log("Product was removed from basket");
        return !isElementPresent(itemInCartName);
    }

    public boolean isBasketEmpty(){
        Reporter.log("Basket is empty");
        return isElementPresentWithWait(2,textOfEmptyBasket);
    }


}
